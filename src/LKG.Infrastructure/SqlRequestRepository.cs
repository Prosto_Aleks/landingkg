﻿using Dapper;
using LKG.Core.Abstractions.Repositories;
using LKG.Core.Entities;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace LKG.Infrastructure
{
    public class SqlRequestRepository : IRequestRepository
    {
        private readonly string _connectionString;

        public SqlRequestRepository(IConfiguration configurations)
        {
            _connectionString = configurations.GetConnectionString("Default");
        }

        public bool Add(Request request)
        {
            using var connection = new NpgsqlConnection(_connectionString);

            string sql = @$"
INSERT INTO public.""requests""(""parentname"", ""parentphone"", ""email"")
VALUES (@parentname, @parentphone, @email)";

            var result = connection.Execute(sql, param: new
            {
                ParentName = request.ParentName,
                ParentPhone = request.ParentPhone,
                Email = request.Email,
            });
            return result > 0;
        }

        public bool Delete(long id)
        {
            using var connection = new NpgsqlConnection(_connectionString);

            var result = connection.Execute($"DELETE FROM public.\"requests\" WHERE Id = {id};");

            return result == 0;
        }

        public Request GetRequestById(long id)
        {
            using var connection = new NpgsqlConnection(_connectionString);

            return connection.QueryFirst<Request>($"SELECT * FROM public.\"requests\" WHERE \"requests\".\"id\" = '{id}'");
        }

        public List<Request> GetRequests()
        {
            using var connection = new NpgsqlConnection(_connectionString);

            return connection.Query<Request>("SELECT * FROM public.\"requests\"").ToList();
        }

        public bool Update(Request request)
        {
            using var connection = new NpgsqlConnection(_connectionString);

            string sql =
                @$"UPDATE public.""requests"" 
                SET parentname = @parentname,
                    parentphone = @parentphone,
                    email = @email
                WHERE id = @id;
                ";

            var result = connection.Execute(sql, param: new
            {
                ParentName = request.ParentName,
                ParentPhone = request.ParentPhone,
                Email = request.Email,
                id = request.Id
            });

            return result == 4;
        }
    }
}
