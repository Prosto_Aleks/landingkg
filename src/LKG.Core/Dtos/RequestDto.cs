﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LKG.Core.Dtos
{
    public class RequestDto
    {
        public long Id { get; set; }
        public string ParentName { get; set; }
        public string ParentPhone { get; set; }
        // not necessary
        public string? Email { get; set; }
        // excursion
        //public bool IsExcursion { get; set; }
    }
}
