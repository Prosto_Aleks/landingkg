﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LKG.Core.Dtos
{
    public class UpdateRequestDto
    {
        public long Id { get; set; }
        public string ParentName { get; set; } = string.Empty;
        public string ParentPhone { get; set; } = string.Empty;
        public string? Email { get; set; } = string.Empty;
    }
}
