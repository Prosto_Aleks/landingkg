﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LKG.Core.Dtos
{
    public class AddRequestDto
    {
        public long Id { get; set; }
        public string ParentName { get; set; }
        public string ParentPhone { get; set; }
        public string? Email { get; set; }

    }
}
