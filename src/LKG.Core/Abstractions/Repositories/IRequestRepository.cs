﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LKG.Core.Entities;

namespace LKG.Core.Abstractions.Repositories
{
    public interface IRequestRepository
    {
        List<Request> GetRequests();
        bool Add(Request request);
        bool Update(Request request);
        bool Delete(long id);
        Request GetRequestById(long id);
    }
}
