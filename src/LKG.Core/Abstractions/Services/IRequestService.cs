﻿using LKG.Core.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LKG.Core.Abstractions
{
    public interface IRequestService
    {
        public List<RequestDto> GetRequests();
        void Add(AddRequestDto addRequestDto);
        void Update(UpdateRequestDto updateRequestDto);
        void Delete(long id);
        RequestDto? GetRequestById(long id);
    }
}
