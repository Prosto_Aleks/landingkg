﻿namespace LKG.Core.Entities
{
    public class Request
    {
        private long _id;
        private string _parentName;
        private string _parentPhone;
        private string? _email;

        public long Id 
        { 
            get => _id; 
            set
            {
                if (value < 0) throw new ArgumentException(nameof(Id));
                _id = value;
            }
        }
        public string ParentName
        {
            get => _parentName;
            set
            {
                if (string.IsNullOrEmpty(value)) throw new ArgumentException(nameof(ParentName));
                _parentName = value;
            }
        }
        public string ParentPhone
        {
            get => _parentPhone;
            set
            {
                if (string.IsNullOrEmpty(value)) throw new ArgumentException(nameof(ParentName));
                _parentPhone = value;
            }
        }
        // not necessary
        public string? Email
        {
            get => _email;
            set
            {
                if (string.IsNullOrEmpty(value)) throw new ArgumentException(nameof(Email));
                _email = value;
            }
        }
        // excursion
        //public bool IsExcursion { get; set; }

        public Request() {}
        public Request(string parentName, string parentPhone, string email, long id = 0)
        {
            if (string.IsNullOrEmpty(parentName) || string.IsNullOrEmpty(parentPhone)
                || string.IsNullOrEmpty(email) || id < 0) throw new ArgumentException();
            ParentName = parentName;
            ParentPhone = parentPhone;
            Email = email;
            Id = id;
        }
    }
}
