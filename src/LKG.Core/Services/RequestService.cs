﻿using LKG.Core.Abstractions;
using LKG.Core.Abstractions.Repositories;
using LKG.Core.Dtos;
using LKG.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LKG.Core.Services
{
    public class RequestService : IRequestService
    {
        private readonly IRequestRepository _repository;

        public RequestService(IRequestRepository requestRepository)
        {
            _repository = requestRepository;
        }

        public void Add(AddRequestDto addRequestDto)
        {
            Request request = new(addRequestDto.ParentName, addRequestDto.ParentPhone, addRequestDto.Email, addRequestDto.Id);
            _repository.Add(request);
        }

        public void Delete(long id)
        {
            var request = _repository.GetRequestById(id);

            _repository.Delete(request.Id);
        }
        public void Update(UpdateRequestDto updateRequestDto)
        {
            var request = _repository.GetRequestById(updateRequestDto.Id);
            request.ParentName = updateRequestDto.ParentName;
            request.ParentPhone = updateRequestDto.ParentPhone;
            request.Email = updateRequestDto.Email;
            _repository.Update(request);
        }

        public List<RequestDto> GetRequests()
        {
            var requests = _repository.GetRequests();
            var dtos = new List<RequestDto>();
            foreach (var request in requests)
            {
                dtos.Add(new RequestDto 
                {
                    Id = request.Id, 
                    ParentName = request.ParentName, 
                    ParentPhone = request.ParentPhone, 
                    Email = request.Email, 
                });
            }
            return dtos;
        }

        public RequestDto? GetRequestById(long id)
        {
            var request = _repository.GetRequestById(id);
            var requestDto = new RequestDto
            {
                Id = request.Id,
                ParentName = request.ParentName,
                ParentPhone = request.ParentPhone,
                Email = request.Email,
            };
            return requestDto;
        }
    }
}
