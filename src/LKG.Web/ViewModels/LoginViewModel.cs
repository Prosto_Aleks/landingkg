﻿using System.ComponentModel.DataAnnotations;

namespace LKG.Web.ViewModels
{
    public class LoginViewModel
    {
        [Required]
        [Display (Name = "Логин")]
        public string Login { get; set; }

        [Required]
        [Display (Name = "Пароль")]
        public string Password { get; set; }
    }
}
