﻿using LKG.Core.Abstractions;
using LKG.Core.Dtos;
using LKG.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace LKG.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IRequestService _service;

        public HomeController(ILogger<HomeController> logger, IRequestService service)
        {
            _logger = logger;
            _service = service;
            
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(AddRequestDto addRequestDto)
        {
           _service.Add(addRequestDto); 
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}