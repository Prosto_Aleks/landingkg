﻿using LKG.Core.Abstractions;
using LKG.Core.Dtos;
using LKG.Web.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace LKG.Web.Controllers
{
    public class AdminPagesController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly IRequestService _service;

        public AdminPagesController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager,
                                    IRequestService service)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _service = service;
        }

        [HttpGet]
        public IActionResult Admin()
        {
            if (User.Identity.IsAuthenticated)
                return AdminMenu();
            return View(viewName: "AdminLogin");
        }

        [HttpPost]
        public async Task<IActionResult> Admin(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(model.Login, model.Password, false, false);
                if (result.Succeeded)
                {
                    return AdminMenu();
                }
                else
                {
                    ModelState.AddModelError("", "Неправильный логин или пароль!");
                }
            } 
            return View(model);
        }

        private IActionResult AdminMenu()
        {
            List<RequestDto> dto = _service.GetRequests();
            return View(viewName: "AdminMenu", model: dto);
        }

        [HttpGet]
        public IActionResult Edit(long id)
        {
            var model = _service.GetRequestById(id);
            return View(viewName: "Edit", model: model);
        }

        [HttpPost]
        public IActionResult Edit(UpdateRequestDto dto)
        {
            _service.Update(dto);
            return AdminMenu();
        }

        [HttpGet]
        public IActionResult Delete(long id)
        {
            _service.Delete(id);
            return AdminMenu();
        }
    }
}
